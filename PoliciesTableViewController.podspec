Pod::Spec.new do |s|
  s.name             = 'PoliciesTableViewController'
  s.version          = '0.1.0'
  s.summary          = 'Create PoliciesTableViewControllerPod.'
  s.homepage         = 'https://gitlab.com/mahvish10/PoliciesTableViewController'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'mahvish10' => 'mahvishsyed15@gmail.com' }
  s.source           = { :git => 'https://gitlab.com/mahvish10/PoliciesTableViewController.git', :tag => s.version.to_s }
  s.ios.deployment_target = '14.0'
  s.swift_version = '4.2'
  s.source_files = 'PoliciesTableViewController/Classes/**/*'
  s.dependency 'SnapKit'
  s.dependency 'PromiseKit', "~> 6.8"
  s.dependency 'Alamofire', '~> 5.2'
  s.dependency 'SwiftyJSON', '~> 4.0'
  s.dependency 'Generics'
  s.dependency 'CustomDropDown'
  s.dependency 'ProfileTableViewController'
  s.dependency 'PopupViewController'
end

